﻿using UnityEngine;
using System.Collections;

public class SizeMenu : MonoBehaviour 
{
	public bool canStart = false;
	public GameObject sizeUI;

	public void SetSize(int size)
	{
		GetComponent<World>().worldX = size;
		GetComponent<World>().worldZ = size;
		GetComponent<World>().start();
		
		canStart = true;
		GameObject.FindGameObjectWithTag("Player").GetComponent<FPSWalkerEnhanced>().enabled = true;
		((MonoBehaviour)GameObject.FindGameObjectWithTag("Player").GetComponent("MouseLook")).enabled = true;
		((MonoBehaviour)GameObject.FindGameObjectWithTag("MainCamera").GetComponent("MouseLook")).enabled = true;
		
		sizeUI.SetActive(false);
	}
}
