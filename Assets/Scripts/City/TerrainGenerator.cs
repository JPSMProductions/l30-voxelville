﻿using UnityEngine;
using System.Collections;

public class TerrainGenerator
{
	public static void generate (City world, CityChunk chunk)
	{
		for (int x = 0; x < world.ChunkX; x++) 
		{
			for (int z = 0; z < world.ChunkZ; z++) 
			{
				int height = 10;

				for (int y = 0; y < height; y++)chunk.SetBlock ( new GrassBlock(), x, y, z);
			}
		}
	}
}
