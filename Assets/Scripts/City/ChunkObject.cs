﻿using UnityEngine;
using System.Collections;

public class ChunkObject : MonoBehaviour 
{
	private City world;
	private CityChunk chunk;
	private MeshFilter meshFilter;
	private MeshCollider meshCollider;
	private bool dirty;
	
	private int lastLevelY;
	
	public static ChunkObject Instance(City world, CityChunk chunk)
	{
		GameObject gameObject = new GameObject("Chunk @(" + chunk.WorldX + "," + chunk.WorldZ + ")");
		gameObject.transform.parent = world.transform;
		gameObject.transform.position = new Vector3(chunk.WorldX, 0, chunk.WorldZ);
		gameObject.transform.rotation = Quaternion.identity;
		gameObject.AddComponent<MeshRenderer>().sharedMaterials = world.materials;
		gameObject.renderer.castShadows = true;
		gameObject.renderer.receiveShadows = true;
		
		ChunkObject chunkObject = gameObject.AddComponent<ChunkObject>();
		chunkObject.Initialize(world, chunk, gameObject.AddComponent<MeshFilter>(), gameObject.AddComponent<MeshCollider>());
		return chunkObject;
	}
	
	public void Initialize(City world, CityChunk chunk, MeshFilter meshFilter, MeshCollider meshCollider)
	{
		this.meshFilter = meshFilter;
		this.meshCollider = meshCollider;
		this.chunk = chunk;
		this.world = world;
		this.lastLevelY = world.LevelY;
	}
	
	public void Update()
	{
		if( chunk.NeighboursReady() && (this.dirty || this.lastLevelY != world.LevelY && renderer.isVisible) )
		{
			meshFilter.sharedMesh = RenderMesh();
			meshCollider.sharedMesh = null;
			meshCollider.sharedMesh = meshFilter.sharedMesh;
			this.lastLevelY = world.LevelY;
			this.dirty = false;
		}
	}
	
	private Mesh RenderMesh()
	{
		world.renderer.Render(world, chunk);
		return world.renderer.ToMesh(meshFilter.sharedMesh);
	}
	
	public void MakeDirty()
	{
		this.dirty = true;
	}
}
