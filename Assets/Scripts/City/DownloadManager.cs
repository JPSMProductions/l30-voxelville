﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;

public class DownloadManager : MonoBehaviour 
{
	public Text headerText;
	public int countPerFrame = 30;
	public bool worldLoaded = false;
	
	private int needToDownload = 0;
	private int downloaded = 0;
	private bool readyToDownload = false;
	private bool nextDownload = false;
	private bool allDownloaded = false;

	private FtpWebRequest request;
	private List<string> fileNames = new List<string>();
	private Hashtable myLevels = new Hashtable();

	void Start()
	{
		connect();
		check();
		count();
	}
	
	private void check()
	{
		string[] files = Directory.GetFiles(@"levels");
		foreach( string f in files )
		{
			string s = f.Replace("levels\\", "");
			myLevels.Add(s, 1);
		}
	}

	void OnGUI()
	{
		if( !readyToDownload ) headerText.text = "Counting levels ("+needToDownload.ToString()+")";
	}
	
	private void connect()
	{
		request = (FtpWebRequest)FtpWebRequest.Create(@"ftp://my-netfriends.de/");
		request.Method = WebRequestMethods.Ftp.ListDirectory;
		request.Credentials = new NetworkCredential(@"web117f1", @"gZehQgd4");
	}
	
	private void count()
	{
		FtpWebResponse response = (FtpWebResponse) request.GetResponse();
		
		Stream responseStream = response.GetResponseStream();
		StreamReader reader = new StreamReader(responseStream);
		
		while( !reader.EndOfStream )
		{
			string name = reader.ReadLine();
			if( name.Equals(".") || name.Equals("..") ) continue;
			
			if( myLevels[name] != null ) continue;
			
			needToDownload++;
			fileNames.Add(name);
		}
		
		readyToDownload = true;
		
		if( needToDownload > 0 ) nextDownload = true;
		else buildCity();
		
		reader.Close();
		response.Close();
	}
	
	private void buildCity()
	{
		allDownloaded = false;
		headerText.text = "Building city...";
		
		GetComponent<World>().start();
		
		createWorld();
	}
	
	private void createWorld()
	{
		headerText.text = "";
		worldLoaded = true;
		
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.GetComponent<FPSWalkerEnhanced>().enabled = true;
		((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
		((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = true;
	}
	
	private void download()
	{
		nextDownload = false;
		headerText.text = "Downloading " + (downloaded + 1) + " of " + needToDownload;
		
		WebClient client = new WebClient();
		client.DownloadFileCompleted += new AsyncCompletedEventHandler(FileDownloaded);
		client.DownloadFileAsync(new System.Uri("http://my-netfriends.de/games/LD30/levels/" + fileNames[0]), @"levels/" + fileNames[0]);
	}
	
	void Update()
	{
		if( readyToDownload && nextDownload ) download();
		if( allDownloaded ) buildCity();
	}
	
	private void FileDownloaded(object sender, AsyncCompletedEventArgs e )
	{
		downloaded++;
		fileNames.RemoveAt(0);
		if( downloaded < needToDownload ) nextDownload = true;
		else allDownloaded = true;
	}
	
	public static string connect(string link)
	{
		return UploadManager.connect(link);
	}
}
