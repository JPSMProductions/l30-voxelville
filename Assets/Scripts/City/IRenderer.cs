﻿using UnityEngine;
using System.Collections;

public interface IRenderer
{
		void Initialize ();
		void Render (City world, CityChunk chunk);
		Mesh ToMesh (Mesh mesh);
}
