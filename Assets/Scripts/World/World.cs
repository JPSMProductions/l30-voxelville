﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using UnityEngine.UI;

public class World : MonoBehaviour 
{
	public bool buildingMode = true;
	public GameObject chunk;
	public Block[,,] data;
	public Chunk[,,] chunks;
	
	public int chunkSize = 16;
	public int worldX = 48;
	public int worldY = 128;
	public int worldZ = 32;
	
	public void start()
	{
		data = new Block[worldX, worldY, worldZ];
		
		if( buildingMode )
		{
			for( int x = 0; x < worldX; x++ )
			{
				for( int z = 0; z < worldZ; z++ )
				{
					data[x, 0, z] = new UndergroundBlock();
				}
			}
		}
		else
		{
			int halfZ = worldZ / 2;
			for( int x = 0; x < worldX; x++ )
			{
				for( int z = 0; z < worldZ; z++ )
				{
					if( z == halfZ || z == halfZ + 1 || z == halfZ - 1) data[x, 0, z] = new StonebrickBlock();
					else data[x, 0, z] = new GrassBlock();
				}
			}
			
			bool left = true;
			int startX = 2;
			
			string[] files = Directory.GetFiles(@"levels");
			
			foreach( string f in files )
			{
				Vector3 pos = new Vector3(startX, 2, halfZ + 4);
				if( !left ) pos.z = halfZ - 4;
				
				GameObject src = Resources.Load("Author Information") as GameObject;
				GameObject go = Instantiate(src, pos, Quaternion.identity) as GameObject;
				if( !left ) go.transform.Rotate(new Vector3(0, 180, 0));
				go.transform.FindChild("Text").GetComponent<Text>().text = "Creator: name";

				pos.y -= .5f;
				GameObject Dgo = Instantiate(src, pos, Quaternion.identity) as GameObject;
				if( !left ) Dgo.transform.Rotate(new Vector3(0, 180, 0));
				Dgo.transform.FindChild("Text").GetComponent<Text>().text = "";
								
				left = !left;
				
				XmlReader reader = XmlReader.Create(@f);
				
				while( reader.Read() )
				{
					if( reader.NodeType == XmlNodeType.Element)
					{
						int xx = 0, yy = 0, zz = 0;
						Block b = new AirBlock();
						
						bool special = false;
						bool isName = true;
						string val = "";
						while( reader.MoveToNextAttribute() )
						{
							if( reader.Name.Equals("s") ) b = Block.createByShortcut(char.Parse(reader.Value));
							else if( reader.Name.Equals("x") ) xx = int.Parse(reader.Value);
							else if( reader.Name.Equals("y") ) yy = int.Parse(reader.Value);
							else if( reader.Name.Equals("z") ) zz = int.Parse(reader.Value);
							else if( reader.Name.Equals("q") )
							{
								special = true;
								if( reader.Value.Equals("d") ) isName = false;
								else isName = true;
							}
							else if( reader.Name.Equals("v") ) val = reader.Value;
						}
						
						if( special && isName ) go.transform.FindChild("Text").GetComponent<Text>().text = "Creator: " + val;
						else if( special ) Dgo.transform.FindChild("Text").GetComponent<Text>().text = val;
						
						
						if( left ) data[startX + xx, yy, halfZ - zz - 4] = b;
						else data[startX + xx, yy, halfZ + zz + 4] = b;
					}
				}
				
				if( left ) startX += 34;
			}
		}
		
		chunks = new Chunk[Mathf.FloorToInt(worldX / chunkSize), Mathf.FloorToInt(worldY / chunkSize), Mathf.FloorToInt(worldZ / chunkSize)];
	}
	
	public void GenColumn(int x, int z)
	{
		for( int y = 0; y < chunks.GetLength(1); y++ ) StartCoroutine(generateChunk(x, y, z));
	}
	
	public IEnumerator generateChunk(int x, int y, int z)
	{
		GameObject newChunk = Instantiate(chunk, new Vector3(x * chunkSize - 0.5f, y * chunkSize + 0.5f, z * chunkSize - 0.5f), Quaternion.identity) as GameObject;
		
		chunks[x, y, z] = newChunk.GetComponent<Chunk>();
		chunks[x, y, z].worldGO = gameObject;
		chunks[x, y, z].chunkSize = chunkSize;
		chunks[x, y, z].chunkX = x * chunkSize;
		chunks[x, y, z].chunkY = y * chunkSize;
		chunks[x, y, z].chunkZ = z * chunkSize;
		
		yield return "";
	}
	
	public void UnloadColumn(int x, int z)
	{
		for( int y = 0; y < chunks.GetLength(1); y++ ) Object.Destroy(chunks[x, y, z].gameObject);
	}
	
	public Block GetBlock(int x, int y, int z)
	{
		if( x >= worldX || x < 0 || y >= worldY || y < 0 || z >= worldZ || z < 0 ) return new AirBlock();
		
		if( data[x, y, z] == null ) return new AirBlock();
		return data[x, y, z];
	}
}
