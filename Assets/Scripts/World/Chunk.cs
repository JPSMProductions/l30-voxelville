﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chunk : MonoBehaviour 
{
	public GameObject worldGO;
	
	public int chunkSize = 16;
	public int chunkX, chunkY, chunkZ;
	
	public bool update;
	
	public List<Vector3> newVertices = new List<Vector3>();
	public List<Vector2> newUV = new List<Vector2>();
	public List<int> newTriangles = new List<int>();
	
	private float tUnit = 0.25f;
	private Mesh mesh;
	private MeshCollider col;
	private World world;
	private int faceCount;
	
	void LateUpdate()
	{
		if( update )
		{
			GenerateMesh();
			update = false;
		}
	}
	
	void Start()
	{
		world = worldGO.GetComponent<World>();
		mesh = GetComponent<MeshFilter>().mesh;
		col = GetComponent<MeshCollider>();
		
		GenerateMesh();
	}
	
	public void GenerateMesh()
	{
		for( int x = 0; x < chunkSize; x++ )
		{
			for( int y = 0; y < chunkSize; y++ )
			{
				for( int z = 0; z < chunkSize; z++ )
				{
					if( GetBlock(x, y, z).ID != 0 )
					{
						if( !GetBlock(x, y, z).HALF )
						{
							if( GetBlock(x, y+1, z).ID == 0 || GetBlock(x, y+1, z).ALPHA ) CubeTop(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y-1, z).ID == 0 || GetBlock(x, y-1, z).ALPHA || GetBlock(x, y-1, z).HALF ) CubeBot(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x+1, y, z).ID == 0 || GetBlock(x+1, y, z).ALPHA || GetBlock(x+1, y, z).HALF ) CubeEast(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x-1, y, z).ID == 0 || GetBlock(x-1, y, z).ALPHA || GetBlock(x-1, y, z).HALF ) CubeWest(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y, z+1).ID == 0 || GetBlock(x, y, z+1).ALPHA || GetBlock(x, y, z+1).HALF ) CubeNorth(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y, z-1).ID == 0 || GetBlock(x, y, z-1).ALPHA || GetBlock(x, y, z-1).HALF ) CubeSouth(x, y, z, GetBlock(x, y, z));
						}
						else
						{
							HalfTop(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y-1, z).ID == 0 || GetBlock(x, y-1, z).ALPHA ) HalfBot(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x+1, y, z).ID == 0 || GetBlock(x+1, y, z).ALPHA ) HalfEast(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x-1, y, z).ID == 0 || GetBlock(x-1, y, z).ALPHA ) HalfWest(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y, z+1).ID == 0 || GetBlock(x, y, z+1).ALPHA ) HalfNorth(x, y, z, GetBlock(x, y, z));
							if( GetBlock(x, y, z-1).ID == 0 || GetBlock(x, y, z-1).ALPHA ) HalfSouth(x, y, z, GetBlock(x, y, z));
						}
					}
				}
			}
		}
		
		UpdateMesh();
	}
	
	void CubeTop(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y, z+1));
		newVertices.Add(new Vector3(x+1, y, z+1));
		newVertices.Add(new Vector3(x+1, y, z));
		newVertices.Add(new Vector3(x, y, z));
		
		Cube(block.UV);

		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z));
			newVertices.Add(new Vector3(x, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void HalfTop(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-.5f, z+1));
		newVertices.Add(new Vector3(x+1, y-.5f, z+1));
		newVertices.Add(new Vector3(x+1, y-.5f, z));
		newVertices.Add(new Vector3(x, y-.5f, z));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z));
			newVertices.Add(new Vector3(x, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void CubeNorth(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		newVertices.Add(new Vector3(x+1, y, z+1));
		newVertices.Add(new Vector3(x, y, z+1));
		newVertices.Add(new Vector3(x, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z+1));
			newVertices.Add(new Vector3(x, y, z+1));
			newVertices.Add(new Vector3(x+1, y, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			
			Cube(block.UV);
		}
	}
	
	void HalfNorth(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		newVertices.Add(new Vector3(x+1, y-.5f, z+1));
		newVertices.Add(new Vector3(x, y-.5f, z+1));
		newVertices.Add(new Vector3(x, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z+1));
			newVertices.Add(new Vector3(x, y-.5f, z+1));
			newVertices.Add(new Vector3(x+1, y-.5f, z+1));
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			
			Cube(block.UV);
		}
	}
	
	void CubeEast(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x+1, y-1, z));
		newVertices.Add(new Vector3(x+1, y, z));
		newVertices.Add(new Vector3(x+1, y, z+1));
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z));
			newVertices.Add(new Vector3(x, y, z));
			newVertices.Add(new Vector3(x, y, z+1));
			newVertices.Add(new Vector3(x, y-1, z+1));
			
			Cube(block.UV);
		}
	}
	
	void HalfEast(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x+1, y-1, z));
		newVertices.Add(new Vector3(x+1, y-.5f, z));
		newVertices.Add(new Vector3(x+1, y-.5f, z+1));
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-1, z));
			newVertices.Add(new Vector3(x, y-.5f, z));
			newVertices.Add(new Vector3(x, y-.5f, z+1));
			newVertices.Add(new Vector3(x, y-1, z+1));
			
			Cube(block.UV);
		}
	}
	
	void CubeSouth(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z));
		newVertices.Add(new Vector3(x, y, z));
		newVertices.Add(new Vector3(x+1, y, z));
		newVertices.Add(new Vector3(x+1, y-1, z));
		
		Cube(block.UV);
		
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x+1, y-1, z));
			newVertices.Add(new Vector3(x+1, y, z));
			newVertices.Add(new Vector3(x, y, z));
			newVertices.Add(new Vector3(x, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void HalfSouth(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z));
		newVertices.Add(new Vector3(x, y-.5f, z));
		newVertices.Add(new Vector3(x+1, y-.5f, z));
		newVertices.Add(new Vector3(x+1, y-1, z));
		
		Cube(block.UV);
		
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x+1, y-1, z));
			newVertices.Add(new Vector3(x+1, y-.5f, z));
			newVertices.Add(new Vector3(x, y-.5f, z));
			newVertices.Add(new Vector3(x, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void CubeWest(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z+1));
		newVertices.Add(new Vector3(x, y, z+1));
		newVertices.Add(new Vector3(x, y, z));
		newVertices.Add(new Vector3(x, y-1, z));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y, z+1));
			newVertices.Add(new Vector3(x+1, y, z));
			newVertices.Add(new Vector3(x+1, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void HalfWest(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z+1));
		newVertices.Add(new Vector3(x, y-.5f, z+1));
		newVertices.Add(new Vector3(x, y-.5f, z));
		newVertices.Add(new Vector3(x, y-1, z));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x+1, y-1, z+1));
			newVertices.Add(new Vector3(x+1, y-.5f, z+1));
			newVertices.Add(new Vector3(x+1, y-.5f, z));
			newVertices.Add(new Vector3(x+1, y-1, z));
			
			Cube(block.UV);
		}
	}
	
	void CubeBot(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z));
		newVertices.Add(new Vector3(x+1, y-1, z));
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		newVertices.Add(new Vector3(x, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y, z));
			newVertices.Add(new Vector3(x+1, y, z));
			newVertices.Add(new Vector3(x+1, y, z+1));
			newVertices.Add(new Vector3(x, y, z+1));
			
			Cube(block.UV);
		}
	}
	
	void HalfBot(int x, int y, int z, Block block)
	{
		newVertices.Add(new Vector3(x, y-1, z));
		newVertices.Add(new Vector3(x+1, y-1, z));
		newVertices.Add(new Vector3(x+1, y-1, z+1));
		newVertices.Add(new Vector3(x, y-1, z+1));
		
		Cube(block.UV);
		
		if( block.ALPHA )
		{
			newVertices.Add(new Vector3(x, y-.5f, z));
			newVertices.Add(new Vector3(x+1, y-.5f, z));
			newVertices.Add(new Vector3(x+1, y-.5f, z+1));
			newVertices.Add(new Vector3(x, y-.5f, z+1));
			
			Cube(block.UV);
		}
	}
	
	void Cube(Vector2 texturePos)
	{
		newTriangles.Add(faceCount * 4);
		newTriangles.Add(faceCount * 4 + 1);
		newTriangles.Add(faceCount * 4 + 2);
		newTriangles.Add(faceCount * 4);
		newTriangles.Add(faceCount * 4 + 2);
		newTriangles.Add(faceCount * 4 + 3);
		
		newUV.Add(new Vector2 (tUnit * texturePos.x + tUnit, tUnit * texturePos.y));
		newUV.Add(new Vector2 (tUnit * texturePos.x + tUnit, tUnit * texturePos.y + tUnit));
		newUV.Add(new Vector2 (tUnit * texturePos.x, tUnit * texturePos.y + tUnit));
		newUV.Add(new Vector2 (tUnit * texturePos.x, tUnit * texturePos.y));
		
		faceCount++;
	}
	
	public Block GetBlock(int x, int y, int z)
	{
		return world.GetBlock(x + chunkX, y + chunkY, z + chunkZ);
	}
	
	void UpdateMesh()
	{
		mesh.Clear();
		mesh.vertices = newVertices.ToArray();
		mesh.uv = newUV.ToArray();
		mesh.triangles = newTriangles.ToArray();
		mesh.Optimize();
		mesh.RecalculateNormals();
		
		col.sharedMesh = null;
		col.sharedMesh = mesh;
		
		newVertices.Clear();
		newUV.Clear();
		newTriangles.Clear();
		
		faceCount = 0;
	}
}
