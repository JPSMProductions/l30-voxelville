﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModifyTerrain : MonoBehaviour 
{
	public Sprite[] blockSprites;

	internal int maxNumber = 14;
	
	private World world;
	private GameObject cameraGO;
	private bool switched = false;
	
	void Start()
	{
		world = gameObject.GetComponent<World>();
		cameraGO = GameObject.FindGameObjectWithTag("MainCamera");
		
		world.start();
	}
	
	void Update()
	{
		if( !GetComponent<Inventory>().isActive && !GetComponent<GameMenu>().isActive && !GetComponent<UploadManager>().SHOWING ) Screen.lockCursor = true;
		else Screen.lockCursor = false;
		
		if( !GetComponent<GameMenu>().isActive && !GetComponent<Inventory>().isActive && Input.GetMouseButtonDown(1) ) AddBlockCenter(5.0f, Block.CreateByID(GetComponent<Inventory>().slots[GetComponent<Inventory>().currentSlot]));
		if( !GetComponent<GameMenu>().isActive && !GetComponent<Inventory>().isActive && Input.GetMouseButtonDown(0) ) ReplaceBlockCenter(5.0f, Block.CreateByID(0));
		
		if( Input.GetKeyDown(KeyCode.Alpha1) ) GetComponent<Inventory>().currentSlot = 0;
		if( Input.GetKeyDown(KeyCode.Alpha2) ) GetComponent<Inventory>().currentSlot = 1;
		if( Input.GetKeyDown(KeyCode.Alpha3) ) GetComponent<Inventory>().currentSlot = 2;
		if( Input.GetKeyDown(KeyCode.Alpha4) ) GetComponent<Inventory>().currentSlot = 3;
		if( Input.GetKeyDown(KeyCode.Alpha5) ) GetComponent<Inventory>().currentSlot = 4;
		if( Input.GetKeyDown(KeyCode.Alpha6) ) GetComponent<Inventory>().currentSlot = 5;
		if( Input.GetKeyDown(KeyCode.Alpha7) ) GetComponent<Inventory>().currentSlot = 6;
		if( Input.GetKeyDown(KeyCode.Alpha8) ) GetComponent<Inventory>().currentSlot = 7;
		if( Input.GetKeyDown(KeyCode.Alpha9) ) GetComponent<Inventory>().currentSlot = 8;
		
		if( !switched )
		{
			if( Input.GetAxis("Mouse ScrollWheel") < 0 )
			{
				GetComponent<Inventory>().currentSlot++;
				if( GetComponent<Inventory>().currentSlot > 8 ) GetComponent<Inventory>().currentSlot = 0;
				switched = true;
			}
			else if( Input.GetAxis("Mouse ScrollWheel") > 0 )
			{
				GetComponent<Inventory>().currentSlot--;
				if( GetComponent<Inventory>().currentSlot < 0 ) GetComponent<Inventory>().currentSlot = 8;
				switched = true;
			}
		}
		else
		{
			if( Input.GetAxis("Mouse ScrollWheel") == 0 ) switched = false;
		}
		
		LoadChunks(GameObject.FindGameObjectWithTag("Player").transform.position, 32, 48);
	}
	
	public void LoadChunks(Vector3 playerPos, float distToLoad, float distToUnload)
	{
		for( int x = 0; x < world.chunks.GetLength(0); x++ )
		{
			for( int z = 0; z < world.chunks.GetLength(2); z++ )
			{
				float dist = Vector2.Distance(new Vector2(x * world.chunkSize, z * world.chunkSize), new Vector2(playerPos.x, playerPos.z));
				
				if( dist < distToLoad )
				{
					if( world.chunks[x, 0, z] == null ) world.GenColumn(x, z);
				}
				else if( dist > distToUnload )
				{
					if( world.chunks[x, 0, z] != null ) world.UnloadColumn(x, z);
				}
			}
		}
	}
	
	public void UpdateChunkAt(int x, int y, int z, Block block)
	{
		int updateX= Mathf.FloorToInt( x/world.chunkSize);
		int updateY= Mathf.FloorToInt( y/world.chunkSize);
		int updateZ= Mathf.FloorToInt( z/world.chunkSize);
		
		world.chunks[updateX,updateY, updateZ].update=true;
		
		if(x-(world.chunkSize*updateX)== 0 && updateX!=0){
			world.chunks[updateX-1,updateY, updateZ].update=true;
		}
		
		if(x-(world.chunkSize*updateX)>=world.chunkSize-1 && updateX!=world.chunks.GetLength(0)-1){
			world.chunks[updateX+1,updateY, updateZ].update=true;
		}
		
		if(y-(world.chunkSize*updateY)==0 && updateY!=0){
			world.chunks[updateX,updateY-1, updateZ].update=true;
		}
		
		if(y-(world.chunkSize*updateY)>=world.chunkSize-1 && updateY!=world.chunks.GetLength(1)-1){
			world.chunks[updateX,updateY+1, updateZ].update=true;
		}
		
		if(z-(world.chunkSize*updateZ)==0 && updateZ!=0){
			world.chunks[updateX,updateY, updateZ-1].update=true;
		}
		
		if(z-(world.chunkSize*updateZ)>=world.chunkSize-1 && updateZ!=world.chunks.GetLength(2)-1){
			world.chunks[updateX,updateY, updateZ+1].update=true;
		}	
	}
	
	public void ReplaceBlockCenter(float range, Block block)
	{
		Ray ray = new Ray(cameraGO.transform.position, cameraGO.transform.forward);
		
		RaycastHit[] hits = Physics.RaycastAll(ray, range);
		if( hits.Length == 0 ) return;
		if( hits[0].collider.name.Contains("Sprite")) return;
		RaycastHit h = hits[0];
		
		foreach( RaycastHit hit in hits )
		{
			if( hit.collider.name.Equals("First Person Controller") ) continue;
			h = hit;
			break;
		}
		
		
		ReplaceBlockAt(h, block);
		
	}
	
	public void AddBlockCenter(float range, Block block)
	{
		Ray ray = new Ray(cameraGO.transform.position, cameraGO.transform.forward);
		// RaycastHit hit;
		
		RaycastHit[] hits = Physics.RaycastAll(ray, range);
		if( hits.Length == 0 ) return;
		RaycastHit h = hits[0];
		
		foreach( RaycastHit hit in hits )
		{
			if( hit.collider.name.Equals("First Person Controller") ) continue;
			h = hit;
			break;
		}
		
		AddBlockAt(h,block);
	}
	
	public void ReplaceBlockCursor(Block block)
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast (ray, out hit)) {
			ReplaceBlockAt(hit, block);
		}
		
	}
	
	public void ReplaceBlockAt(RaycastHit hit, Block block) 
	{
		Vector3 position = hit.point;
		position+=(hit.normal*-0.5f);
		
		if( world.GetBlock(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), Mathf.RoundToInt(position.z)).ID == -1 ) return;
	
		SoundManager sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundManager>();
		if( GetComponent<Inventory>().slots[GetComponent<Inventory>().currentSlot] != 0 ) sm.PlaySound(sm.breakSound);
			
		SetBlockAt(position, block);
	}
	
	public void ReplaceBlockAt(Vector3 pos, Block block) 
	{
		Vector3 position = pos;
		position+=(pos.normalized*-0.5f);
		
		if( world.GetBlock(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), Mathf.RoundToInt(position.z)).ID == -1 ) return;
		
		SoundManager sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundManager>();
		if( GetComponent<Inventory>().slots[GetComponent<Inventory>().currentSlot] != 0 ) sm.PlaySound(sm.breakSound);
		
		SetBlockAt(position, block);
	}
	
	public void AddBlockAt(RaycastHit hit, Block block) 
	{
		Vector3 position = hit.point;
		position+=(hit.normal*0.5f);

		SoundManager sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundManager>();
		if( GetComponent<Inventory>().slots[GetComponent<Inventory>().currentSlot] != 0 ) sm.PlaySound(sm.placeSound);
				
		SetBlockAt(position,block);		
	}
	
	public void AddBlockAt(Vector3 pos, Block block) 
	{
		Vector3 position = pos;
		position+=(pos.normalized*0.5f);
		
		SoundManager sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundManager>();
		if( GetComponent<Inventory>().slots[GetComponent<Inventory>().currentSlot] != 0 ) sm.PlaySound(sm.placeSound);
		
		SetBlockAt(position,block);
	}
	
	public void SetBlockAt(Vector3 position, Block block) 
	{
		int x= Mathf.RoundToInt( position.x );
		int y= Mathf.RoundToInt( position.y );
		int z= Mathf.RoundToInt( position.z );
		
		SetBlockAt(x,y,z,block);
	}
	
	public void SetBlockAt(int x, int y, int z, Block block) 
	{
		if( block.ID == 0 && world.GetBlock(x, y-1, z).HALF ) y--;
		
		if( block.ID != 0 && world.GetBlock(x, y, z).HALF )
		{
			if( block.ID == world.GetBlock(x, y-1, z).ID )
			{
				block = block.ToComplete();
				y--;
			}
		}
		
		if( block.ID != 0 && world.GetBlock(x, y-1, z).HALF )
		{
			if( block.ID == world.GetBlock(x, y-1, z).ID )
			{
				block = block.ToComplete();
				y--;
			}
			else y++;
		}
		
		world.data[x,y,z] = block;
		UpdateChunkAt(x,y,z,block);
		
	}
}
