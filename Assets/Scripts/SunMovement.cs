﻿using UnityEngine;
using System.Collections;

public class SunMovement : MonoBehaviour 
{
	public float speed = 10f;
	
	void Update()
	{
		transform.Rotate(new Vector3(Time.deltaTime * speed, Time.deltaTime * speed, 0));
	}
}
