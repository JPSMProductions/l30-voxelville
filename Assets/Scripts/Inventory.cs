﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour 
{
	public GUISkin btnSkin;
	public Texture2D backgroundImage;
	public Texture2D slotImage;
	public Texture2D slotbar;
	public Texture2D slotbarSlot;
	public Texture2D slotbarSlotSelected;
	public Texture2D[] blocks;
	public Texture2D[] blocksBIG;
	public int[] slots = new int[9];
	public int currentSlot = 0;
				
	public bool isActive = false;

	private ModifyTerrain terrain;
	private int selectedBlock = 0;
	
	void Start()
	{
		terrain = GetComponent<ModifyTerrain>();
	}
		
	void Update()
	{
		if( GetComponent<GameMenu>().isActive ) return;
		
		if( Input.GetKeyDown(KeyCode.Escape) && isActive )
		{
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			player.GetComponent<FPSWalkerEnhanced>().enabled = true;
			((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
			((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = true;
			isActive = false;
			
		}
		
		if( Input.GetKeyDown(KeyCode.E) )
		{
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			if( isActive )
			{
				player.GetComponent<FPSWalkerEnhanced>().enabled = true;
				((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
				((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = true;
			}
			else
			{
				player.GetComponent<FPSWalkerEnhanced>().enabled = false;
				((MonoBehaviour)player.GetComponent("MouseLook")).enabled = false;
				((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = false;
			}
			
			isActive = !isActive;
		}
	}
	
	
	void OnGUI()
	{
		int w = Screen.width;
		int h = Screen.height;
		
		if( isActive )
		{
			int slotSize = 52;
			
			GUI.DrawTexture(new Rect(w / 2 - 400, h / 2 - 300, 800, 600), backgroundImage);
			
			int slotsInLine = 12;
			int startX = (w / 2 - 400) + 56;
			int startY = (h / 2 - 300) + 100;
			
			int x = 0, y = 0;
			for( int i = 1; i <= terrain.maxNumber; i++ )
			{
				GUI.DrawTexture(new Rect(startX + x*slotSize + x*5, startY + y*slotSize + y*5, slotSize, slotSize), slotImage);
				if( GUI.Button(new Rect(startX + x*slotSize + x*5, startY + y*slotSize + y*5, slotSize, slotSize), blocks[i], btnSkin.button) ) selectedBlock = i;
				x++;
				
				if( x >= slotsInLine )
				{
					x = 0;
					y++;
				}
			}
		}
		
		GUI.DrawTexture(new Rect(w / 2 - 400 - 2, Screen.height - slotbar.height, 800, slotbar.height), slotbar);
		
		int StartX = (w / 2 - 400) + 56 + 12;
		int StartY = Screen.height - slotbar.height + 27;
		int slotbarSlotNum = 9;
		int s = 0;
		for( int xx = 0; xx < slotbarSlotNum; xx++ )
		{
			if( s == currentSlot ) GUI.DrawTexture(new Rect(StartX + xx*64 + xx*10, StartY, 64, 64), slotbarSlotSelected);
			else GUI.DrawTexture(new Rect(StartX + xx*64 + xx*10, StartY, 64, 64), slotbarSlot);
			
			if( slots[s] != 0 )
			{
				if( GUI.Button(new Rect(StartX + xx*64 + xx*10, StartY, 64, 64), blocksBIG[slots[s]], btnSkin.button) )
				{
					if( selectedBlock > 0 )
					{
						slots[s] = selectedBlock;
						selectedBlock = 0;
					}
				}
			}
			else
			{
				if( GUI.Button(new Rect(StartX + xx*64 + xx*10, StartY, 64, 64), "", btnSkin.button) )
				{
					if( selectedBlock > 0 )
					{
						slots[s] = selectedBlock;
						selectedBlock = 0;
					}
				}
			}
			s++;
		}
		
		if( selectedBlock > 0 )
		{
			float posX = Input.mousePosition.x;
			float posY = Screen.height - Input.mousePosition.y;
			GUI.DrawTexture(new Rect(posX, posY, 48, 48), blocksBIG[selectedBlock]);
		}
	}
}
