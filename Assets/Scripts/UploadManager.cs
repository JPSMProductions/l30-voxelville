﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;

public class UploadManager : MonoBehaviour 
{
	public GameObject cursorGO;
	public GameObject uploadGO;
	public GameObject warningGO;
	
	public InputField nameField;
	public InputField descField;
	
	private bool showForm = false;
	
	public bool SHOWING { get { return showForm; } }
	
	public void show()
	{
		showForm = true;
		cursorGO.SetActive(false);
		
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.GetComponent<FPSWalkerEnhanced>().enabled = false;
		GetComponent<GameMenu>().enabled = false;
		GetComponent<Inventory>().enabled = false;
		((MonoBehaviour)player.GetComponent("MouseLook")).enabled = false;
		((MonoBehaviour)GameObject.FindGameObjectWithTag("MainCamera").GetComponent("MouseLook")).enabled = false;
		
		GetComponent<GameMenu>().isActive = false;
		uploadGO.SetActive(true);
	}
	
	public void hide()
	{
		showForm = false;
		cursorGO.SetActive(true);
		
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.GetComponent<FPSWalkerEnhanced>().enabled = true;
		GetComponent<GameMenu>().enabled = true;
		GetComponent<Inventory>().enabled = true;
		((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
		((MonoBehaviour)GameObject.FindGameObjectWithTag("MainCamera").GetComponent("MouseLook")).enabled = true;
		
		uploadGO.SetActive(false);
	}
	
	void Update()
	{
		if( SHOWING )
		{
			if( Input.GetKeyDown(KeyCode.Escape) )
				hide();
		}
	}
	
	public void startUpload()
	{
		if( nameField.value.Equals("") )
		{
			warningGO.SetActive(true);
			return;
		}
		
		warningGO.SetActive(false);
		
		World world = GetComponent<World>();
		
		XmlWriter writer = XmlWriter.Create(@"temp.xml");
		writer.WriteStartElement("a");
		
			writer.WriteStartElement("t", "");
				writer.WriteAttributeString("q", "n");
				writer.WriteAttributeString("v", nameField.value);
			writer.WriteEndElement();
			
			writer.WriteStartElement("t", "");
				writer.WriteAttributeString("q", "d");
				writer.WriteAttributeString("v", descField.value);
			writer.WriteEndElement();
		
			for( int x = 0; x < world.worldX; x++ )
			{
				for( int y = 0; y < world.worldY; y++ )
				{
					for( int z = 0; z < world.worldZ; z++ )
					{
						if( world.GetBlock(x, y, z).ID != 0 ) 
						{
							writer.WriteStartElement("b", "");
								writer.WriteAttributeString("s", world.GetBlock(x, y, z).SHORTCUT.ToString()); // shortcut
								writer.WriteAttributeString("x", x.ToString());
								writer.WriteAttributeString("y", y.ToString());
								writer.WriteAttributeString("z", z.ToString());
							writer.WriteEndElement();
						
						}
					}
				}
			}
			
		writer.WriteEndElement();
		writer.Close();

		string fileName = "lvl_";
		int numCount = 5;
		string id = "";
		
		for( int i = 0; i < numCount; i++ ) id += Random.Range(0, 9).ToString();
		
		fileName += id;
		fileName += ".xml";

		Debug.Log("Upload Filename: " + fileName);

		FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(@"ftp://www.my-netfriends.de/" + fileName);
		request.Method = WebRequestMethods.Ftp.UploadFile;
		request.Credentials = new NetworkCredential(@"web117f1", @"gZehQgd4");
		
		StreamReader sourceStream = new StreamReader(@"temp.xml");
		byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
		sourceStream.Close();
		request.ContentLength = fileContents.Length;
		
		Stream requestStream = request.GetRequestStream();
		requestStream.Write(fileContents, 0, fileContents.Length);
		requestStream.Close();
		
		FtpWebResponse response = (FtpWebResponse)request.GetResponse();
		Debug.Log("FTP-Status: " + response.StatusDescription);
		response.Close();

		string path = "http://my-netfriends.de/games/LD30/addfile.php?id=" + id + "&name=" + nameField.value + "&description=" + descField.value;
		string connection = connect(path);
		if( !connection.Equals("true") )
		{
			Debug.LogError("Web Error: " + connection);
		}
		
		PlayerPrefs.SetString("msg", "Level uploaded successfully!");
		Application.LoadLevel(0);
	}
	
	public static string connect(string link)
	{
		WebClient client = new WebClient();
		return client.DownloadString(link);
	}
}
