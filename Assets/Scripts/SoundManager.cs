﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class SoundManager : MonoBehaviour 
{
	public AudioClip placeSound;
	public AudioClip breakSound;
	
	private AudioSource audio;
	
	void Start()
	{
		audio = GetComponent<AudioSource>();
	}
	
	public void PlaySound(AudioClip clip)
	{
		audio.clip = clip;
		audio.Play();
	}
}
