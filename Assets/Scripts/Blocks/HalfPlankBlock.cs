﻿using UnityEngine;
using System.Collections;

public class HalfPlankBlock : Block
{
	public HalfPlankBlock() : base(ID_HALF_PLANK, new Vector2(3, 0), 'w')
	{
		this.half = true;
	}
	
	public override Block ToComplete ()
	{
		return new PlankBlock();
	}
}
