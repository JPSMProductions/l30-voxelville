﻿using UnityEngine;
using System.Collections;

public class AirBlock : Block 
{
	public AirBlock() : base(ID_AIR, Vector2.zero, '0') {}
}
