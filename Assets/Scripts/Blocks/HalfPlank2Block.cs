﻿using UnityEngine;
using System.Collections;

public class HalfPlank2Block : Block
{
	public HalfPlank2Block() : base(ID_HALF_PLANK2, new Vector2(2, 1), 'q')
	{
		this.half = true;
	}
	
	public override Block ToComplete ()
	{
		return new Plank2Block();
	}
}
