﻿using UnityEngine;
using System.Collections;

public class Block
{
	/* ID Stuff */
	public static int ID_UNDERGROUND = -1;
	public static int ID_AIR = 0;
	public static int ID_STONE = 1;
	public static int ID_GRASS = 2;
	public static int ID_DIRT = 3;
	public static int ID_PLANK = 4;
	public static int ID_SAND = 5;
	public static int ID_PLANK2 = 6;
	public static int ID_STONEBRICK = 7;
	public static int ID_GLASS = 8;
	public static int ID_WOOD = 9;
	public static int ID_LEAVE = 10;
	public static int ID_HALF_PLANK = 11;
	public static int ID_HALF_STONE = 12;
	public static int ID_HALF_PLANK2 = 13;
	public static int ID_HALF_STONEBRICK = 14;
	/* END */
	
	protected Vector2 uvTextures = Vector2.zero;
	protected int id = 0;
	protected bool alpha = false;
	protected bool half = false;
	protected char shortcut;
	
	public Vector2 UV { get { return uvTextures; }}
	public int ID { get { return id; }}
	public bool ALPHA { get { return alpha; }}
	public bool HALF { get { return half; }}
	public char SHORTCUT { get { return shortcut; }}
	
	public bool top = false;
	
	public Block(int id, Vector2 uv, char shortcut)
	{
		this.id = id;
		this.uvTextures = uv;
		this.shortcut = shortcut;
	}
	
	public static Block CreateByID(int blockID)
	{
		switch( blockID )
		{
			case 1: return new StoneBlock();
			case 2: return new GrassBlock();
			case 3: return new DirtBlock();
			case 4: return new PlankBlock();
			case 5: return new SandBlock();
			case 6: return new Plank2Block();
			case 7: return new StonebrickBlock();
			case 8: return new GlassBlock();
			case 9: return new WoodBlock();
			case 10: return new LeaveBlock();
			case 11: return new HalfPlankBlock();
			case 12: return new HalfStoneBlock();
			case 13: return new HalfPlank2Block();
			case 14: return new HalfStonebrickBlock();
		}
		
		return new AirBlock();
	}
	
	public static Block createByShortcut(char blockShortCut)
	{
		switch( blockShortCut )
		{
			case 'D': return new DirtBlock();
			case 'G': return new GlassBlock();
			case '1': return new GrassBlock();
			case 'q': return new HalfPlank2Block();
			case 'w': return new HalfPlankBlock();
			case 'e': return new HalfStoneBlock();
			case 'r': return new HalfStonebrickBlock();
			case 'L': return new LeaveBlock();
			case 'P': return new Plank2Block();
			case 'O': return new PlankBlock();
			case 'S': return new SandBlock();
			case 'A': return new StoneBlock();
			case 'C': return new StonebrickBlock();
			case '_': return new UndergroundBlock();
			case 'W': return new WoodBlock();
		}
		
		return new AirBlock();
	}
	
	public virtual Block ToComplete()
	{
		return new AirBlock();
	}
}
