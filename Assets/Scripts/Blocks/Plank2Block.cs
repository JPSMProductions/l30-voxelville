﻿using UnityEngine;
using System.Collections;

public class Plank2Block : Block
{
	public Plank2Block() : base(ID_PLANK2, new Vector2(2, 1), 'P')
	{
	}
}
