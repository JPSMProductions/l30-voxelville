﻿using UnityEngine;
using System.Collections;

public class UndergroundBlock : Block
{
	public UndergroundBlock() : base(ID_UNDERGROUND, new Vector2(0, 1), '_')
	{
	}
}
