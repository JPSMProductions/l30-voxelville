﻿using UnityEngine;
using System.Collections;

public class StonebrickBlock : Block
{
	public StonebrickBlock() : base(ID_STONEBRICK, new Vector2(3, 1), 'C')
	{
	}
}
