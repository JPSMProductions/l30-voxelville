﻿using UnityEngine;
using System.Collections;

public class GlassBlock : Block
{
	public GlassBlock() : base(ID_GLASS, new Vector2(0, 2), 'G')
	{
		alpha = true;
	}
}
