﻿using UnityEngine;
using System.Collections;

public class HalfStonebrickBlock : Block
{
	public HalfStonebrickBlock() : base(ID_HALF_STONEBRICK, new Vector2(3, 1), 'r')
	{
		this.half = true;
	}
	
	public override Block ToComplete ()
	{
		return new StonebrickBlock();
	}
}
