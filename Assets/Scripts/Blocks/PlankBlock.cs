﻿using UnityEngine;
using System.Collections;

public class PlankBlock : Block
{
	public PlankBlock() : base(ID_PLANK, new Vector2(3, 0), 'O')
	{
	}
}
