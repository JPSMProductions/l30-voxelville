﻿using UnityEngine;
using System.Collections;

public class HalfStoneBlock : Block
{
	public HalfStoneBlock() : base(ID_HALF_STONE, new Vector2(0, 0), 'e')
	{
		this.half = true;
	}
	
	public override Block ToComplete ()
	{
		return new StoneBlock();
	}
}
