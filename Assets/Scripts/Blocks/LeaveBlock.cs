﻿using UnityEngine;
using System.Collections;

public class LeaveBlock : Block
{
	public LeaveBlock() : base(ID_LEAVE, new Vector2(2, 2), 'L')
	{
		alpha = true;
	}
}
