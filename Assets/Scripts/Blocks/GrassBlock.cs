﻿using UnityEngine;
using System.Collections;

public class GrassBlock : Block
{
	public GrassBlock() : base(ID_GRASS, new Vector2(1, 0), '1')
	{
	}
}
