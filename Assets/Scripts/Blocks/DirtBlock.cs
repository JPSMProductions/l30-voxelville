﻿using UnityEngine;
using System.Collections;

public class DirtBlock : Block
{
	public DirtBlock() : base(ID_DIRT, new Vector2(2, 0), 'D') {}
}
