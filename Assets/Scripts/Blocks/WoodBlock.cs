﻿using UnityEngine;
using System.Collections;

public class WoodBlock : Block
{
	public WoodBlock() : base(ID_WOOD, new Vector2(1, 2), 'W')
	{
	}
}
