﻿using UnityEngine;
using System.Collections;

public class SandBlock : Block
{
	public SandBlock() : base(ID_SAND, new Vector2(1, 1), 'S')
	{
	}
}
