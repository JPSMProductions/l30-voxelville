﻿using UnityEngine;
using System.Collections;

public class GameMenuCity : MonoBehaviour 
{
	public bool isActive = false;
	
	void Update()
	{
		if( !GetComponent<DownloadManager>().worldLoaded ) return;
	
		if( Input.GetKeyDown(KeyCode.Escape) )
		{
			GameObject player = GameObject.FindGameObjectWithTag("Player");
			if( isActive )
			{
				player.GetComponent<FPSWalkerEnhanced>().enabled = true;
				((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
				((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = true;
			}
			else
			{
				player.GetComponent<FPSWalkerEnhanced>().enabled = false;
				((MonoBehaviour)player.GetComponent("MouseLook")).enabled = false;
				((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = false;
			}
			
			isActive = !isActive;
		}
	}
	
	void OnGUI()
	{
		if( isActive )
		{
			int w = Screen.width;
			int h = Screen.height;
			
			int btnWidth = 250;
			int btnHeight = 25;
			
			int boxWidth = btnWidth + 20;
			int boxHeight = btnHeight*4 + 40 + 60; // space 10 pixels
			
			GUI.BeginGroup(new Rect((w / 2) - (boxWidth / 2), (h / 2) - (boxHeight / 2), boxWidth, boxHeight));
				GUI.Box(new Rect(0, 0, boxWidth, boxHeight), "Game menu");
				
				if( GUI.Button(new Rect(10, 40, btnWidth, btnHeight), "Continue game") )
				{
					GameObject player = GameObject.FindGameObjectWithTag("Player");
					player.GetComponent<FPSWalkerEnhanced>().enabled = true;
					((MonoBehaviour)player.GetComponent("MouseLook")).enabled = true;
					((MonoBehaviour)player.transform.FindChild("Main Camera").GetComponent("MouseLook")).enabled = true;
					isActive = false;
				}
				
				if( GUI.Button(new Rect(10, 40 + btnHeight + 10, btnWidth, btnHeight), "Build a house") ) Application.LoadLevel(1);
				
				if( GUI.Button(new Rect(10, 40 + btnHeight*2 + 20, btnWidth, btnHeight), "Go to titlescreen") ) Application.LoadLevel(0);
				if( GUI.Button(new Rect(10, 40 + btnHeight*3 + 30, btnWidth, btnHeight), "Exit game") ) Application.Quit();
			GUI.EndGroup();
		}
	}
}
