﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour 
{
	public Text infoMsg;

	private string infoTxt = "";

	public void BuildMode()
	{
		Application.LoadLevel(1);
	}
	
	public void ExitGame()
	{
		Application.Quit();
	}
	
	public void ShowCredits()
	{
		Application.LoadLevel(2);
	}
	
	public void ToMenu()
	{
		Application.LoadLevel(0);
	}
	
	public void ToCity()
	{
		Application.LoadLevel(3);
	}
	
	void Start()
	{
		if( PlayerPrefs.GetString("msg") != null && PlayerPrefs.GetString("msg") != "" )
		{
			infoTxt = PlayerPrefs.GetString("msg");
			PlayerPrefs.DeleteAll();
		}
	}	
	
	void Update()
	{
		if( infoMsg == null ) return;
		
		infoMsg.text = infoTxt;
	}
}
